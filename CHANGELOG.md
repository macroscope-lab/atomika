## v0.19.5 (2024-08-08)

### Fix

- remove redis pkg generic

### Refactor

- transport register method

## v0.19.4 (2024-07-31)

### Fix

- error messages output to string

## v0.19.3 (2024-07-30)

### Fix

- version trigger

### Refactor

- all moduels accessed the same way

## v0.19.2 (2024-07-29)

### Refactor

- moved framework to project root

## v0.19.1 (2024-07-24)

### Fix

- ssh key parsing
- nil panic when config not specified

## v0.19.0 (2024-05-24)

### Feat

- update golang to 1.22.3
- update client sdks. cleanup
- add js and ts clients
- add UI binder for transpot
- add golang sdk client

### Refactor

- pkgs config loading. cleanup
- configuration loading to static functions
- move sdk dir to project root

## v0.18.2 (2024-02-15)

### Refactor

- convert types to iota to avoid ctx collision

## v0.18.1 (2024-02-13)

### Fix

- add path flag

## v0.18.0 (2024-02-08)

### Feat

- migrated to new config

### Fix

- critic linter moved to config

## v0.17.0 (2024-02-08)

### Feat

- add default interface for config

## v0.16.1 (2024-02-08)

### Fix

- config handle string, int and bool correctly

## v0.16.0 (2024-02-07)

### Feat

- add config key data type

### Fix

- remove ci goreleaser

## v0.15.0 (2024-02-07)

### Feat

- replace viper env bind with custom bind

### Fix

- missing files

## v0.14.1 (2024-02-05)

### Refactor

- remove docker flag as not needed
- remove docker flag as not needed

## v0.14.0 (2024-02-01)

### Feat

- add run command
- add command

### Fix

- Dockerfile data parse

## v0.13.0 (2024-01-31)

### Feat

- add redis support
- add Z sorted set support
- add redis kv store
- add basic redis cluster support
- add more structure files
- **templates**: add error, service and service_test
- add project create and service create

### Fix

- templates paths
- var types in templates

### Refactor

- template parse with metadata
- **runtime**: deprecate autoconf for db. remove store pkg
- **runtime**: deprecate autoconf for log
- **runtime**: deprecate autoconf for transport

## v0.12.11 (2023-07-19)

### Fix

- version match in go.mod

## v0.12.10 (2023-07-19)

### Refactor

- new structure

## v0.12.9 (2023-06-16)

### Fix

- update CI pipx PATH
- **ci**: new python3 installation
- **runtime**: revert changes in runtime config loading

## v0.12.8 (2023-04-28)

### Fix

- better explanation for di package
- **transport**: handle errors
- use const var instead of string value

### Refactor

- **transport**: removed ws support

## v0.12.7 (2023-03-30)

### Fix

- typos

## v0.12.6 (2023-03-30)

### Fix

- config key selection
- doc typos

### Refactor

- remove workspace object

## v0.12.5 (2023-02-13)

### Fix

- **logger**: fix log runtime cfg

### Refactor

- **transport**: remove shutdown log messages

## v0.12.4 (2023-01-26)

### Fix

- **transport**: error response

## v0.12.3 (2023-01-26)

### Fix

- transport response and error response

## v0.12.2 (2023-01-25)

### Refactor

- **transport**: remove http codes

## v0.12.1 (2023-01-19)

### Refactor

- cleanup unneeded uuid validation

## v0.12.0 (2023-01-19)

### Feat

- **types**: add uuid validator

## v0.11.2 (2022-12-28)

### Fix

- **transport**: cors

### Refactor

- **stringer**: RandomString function

## v0.11.1 (2022-12-25)

### Refactor

- **transport**: add interceptor cascading per method

## v0.11.0 (2022-12-22)

### Feat

- **transport**: add traceKey to req context

### Fix

- **runtime**: add missing log configuration

## v0.10.3 (2022-12-22)

### Refactor

- **transport**: expose error as response

## v0.10.2 (2022-12-20)

### Fix

- **runtime**: db config key conflict

## v0.10.1 (2022-12-19)

### Refactor

- **runtime**: add viper pkg

## v0.10.0 (2022-12-11)

### Feat

- **db**: Add basic db support
- add basic log
- add config for db, log and ws
- add interceptor and ws support

### Fix

- **di**: remove interface. made storage private
- log configuration

## v0.9.0 (2022-11-15)

### Feat

- **transport**: add new codes

## v0.8.0 (2022-11-08)

### Feat

- add ws response decode

## v0.7.0 (2022-09-30)

### Feat

- **store**: add local store
- **stringer**: add pkg
- **runtime**: add setup step
- **log**: add log support
- **transport**: add ws support
- **types**: add uuid

### Refactor

- add log

## v0.6.0 (2022-08-29)

### Feat

- **transport**: add interceptor

### Refactor

- **di**: di storage

## v0.5.1 (2022-08-24)

### Refactor

- **transport**: client handling

## v0.5.0 (2022-08-02)

### Feat

- **transport**: add cors

### Fix

- **runtime**: add cors and www support
- **di**: add service.json

## v0.4.0 (2022-07-20)

### Feat

- add component interface
- **di**: add

## v0.3.0 (2022-07-14)

### Feat

- **tag**: add env omitempty support

## v0.2.0 (2022-07-06)

### Feat

- add tag auto parse
- add tag support

### Fix

- unit tests

### Refactor

- config loading with custom data

## v0.1.0 (2022-06-09)

### Feat

- **transport**: add route validation
- add transport
- **testdata**: add pkg
- **runtime**: add pkg
- **transport**: add pkg
- add CI tools

### Refactor

- add context

## v0.0.1 (2022-06-02)
