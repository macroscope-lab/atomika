package atomika

import (
	"context"
	"os"
	"os/signal"
	"syscall"
	
	"golang.org/x/sync/errgroup"
)

type Atomika interface {
	RegisterService([]Service)
	Boot() error
}

type atomika struct {
	services []Service
}

func New() Atomika {
	return &atomika{}
}

func (a *atomika) RegisterService(services []Service) {
	a.services = append(a.services, services...)
}

func (a *atomika) Boot() error {
	
	stopCh, closeCh := shutdown()
	defer closeCh()
	
	ctx, cancel := context.WithCancel(context.Background())
	
	go func(cancel context.CancelFunc) {
		<-stopCh
		cancel()
	}(cancel)
	
	eg, egCtx := errgroup.WithContext(ctx)
	for _, s := range a.services {
		service := s
		eg.Go(func() error {
			return service.Run(egCtx)
		})
	}
	
	return eg.Wait()
}

func shutdown() (chan os.Signal, func()) {
	stopChan := make(chan os.Signal, 1)
	signal.Notify(stopChan, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)
	
	return stopChan, func() {
		close(stopChan)
	}
}
