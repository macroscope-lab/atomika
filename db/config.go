package db

import (
	"gitlab.com/macroscope-lab/atomika/runtime"
)

type Config struct {
	Driver      string `mapstructure:"dbDriver" envVar:"ATMK_DB_DRIVER"`
	Host        string `mapstructure:"dbHost" envVar:"ATMK_DB_HOST"`
	Port        string `mapstructure:"dbPort" envVar:"ATMK_DB_PORT"`
	User        string `mapstructure:"dbUser" envVar:"ATMK_DB_USER"`
	Pass        string `mapstructure:"dbPass" envVar:"ATMK_DB_PASS"`
	Name        string `mapstructure:"dbName" envVar:"ATMK_DB_NAME"`
	Schema      string `mapstructure:"dbSchema" envVar:"ATMK_DB_SCHEMA"`
	SSL         bool   `mapstructure:"dbSSL" envVar:"ATMK_DB_SSL"`
	PingTimeout int    `mapstructure:"dbPingTimeout" envVar:"ATMK_DB_TIMEOUT"`
}

func Configure(key ...string) (*Config, error) {
	c := &Config{}
	if err := runtime.Get(c, key...); err != nil {
		return nil, err
	}
	return c, nil
}

func Defaults() *Config {
	return &Config{
		PingTimeout: 30,
	}
}

func mergeWithDefaults(config *Config) *Config {
	cfg := Defaults()
	_ = runtime.MergeConfigWithDefaults[*Config](config, cfg)
	
	return config
}
