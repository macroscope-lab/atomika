package db

import (
	"database/sql"
	"fmt"
)

const (
	DriverPostgres = "postgres"
	DriverMySQL    = "mysql"
	DriverSQLite   = "sqlite"
)

type DB interface {
	Connect() (*sql.DB, error)
	Ping() error
	// Add other common DB operations if needed
}

func New(cfg *Config) (DB, error) {
	var err error
	if cfg == nil {
		cfg, err = Configure()
	}
	if err != nil {
		return nil, err
	}
	
	mergeWithDefaults(cfg)
	
	switch cfg.Driver {
	case DriverPostgres:
		return newPostgresDB(cfg)
	case DriverMySQL:
		return newMySqlDB(cfg)
	case DriverSQLite:
		return newSqliteDB(cfg)
	default:
		return nil, fmt.Errorf("%w:[%s]", ErrUnknownDriver, cfg.Driver)
	}
}

func newPostgresDB(cfg *Config) (*PostgresDB, error) {
	return &PostgresDB{
		config: cfg,
	}, nil
}

func newMySqlDB(cfg *Config) (*MysqlDB, error) {
	return &MysqlDB{
		config: cfg,
	}, nil
}

func newSqliteDB(cfg *Config) (*SqliteDB, error) {
	return &SqliteDB{
		config: cfg,
	}, nil
	
}
