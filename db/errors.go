package db

import (
	"errors"
)

var (
	ErrUnknownDriver = errors.New("unknown driver")
)
