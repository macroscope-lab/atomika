package db

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

type MysqlDB struct {
	config *Config
}

func (mysqlDB *MysqlDB) Connect() (*sql.DB, error) {
	return sql.Open("mysql", mysqlDB.dsn())
}

func (mysqlDB *MysqlDB) Ping() error {
	db, err := mysqlDB.Connect()
	if err != nil {
		return err
	}

	defer func(db *sql.DB) {
		_ = db.Close()
	}(db)

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(mysqlDB.config.PingTimeout)*time.Second)
	defer cancel()

	return db.PingContext(ctx)
}

func (mysqlDB *MysqlDB) dsn() string {
	return fmt.Sprintf("mysql://%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		mysqlDB.config.User,
		mysqlDB.config.Pass,
		mysqlDB.config.Host,
		mysqlDB.config.Port,
		mysqlDB.config.Name,
	)
}
