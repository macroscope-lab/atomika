package db

import (
	"context"
	"database/sql"
	"fmt"
	"time"
	
	_ "github.com/jackc/pgx/v5/stdlib"
)

type PostgresDB struct {
	config *Config
}

func (postgresDB *PostgresDB) Connect() (*sql.DB, error) {
	return sql.Open("pgx", postgresDB.dsn())
}

func (postgresDB *PostgresDB) Ping() error {
	db, err := postgresDB.Connect()
	if err != nil {
		return err
	}
	
	defer func(db *sql.DB) {
		_ = db.Close()
	}(db)
	
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(postgresDB.config.PingTimeout)*time.Second)
	defer cancel()
	
	return db.PingContext(ctx)
}

func (postgresDB *PostgresDB) dsn() string {
	return fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=%s",
		postgresDB.config.User,
		postgresDB.config.Pass,
		postgresDB.config.Host,
		postgresDB.config.Port,
		postgresDB.config.Name,
		sslMode(postgresDB.config.SSL),
	)
}

func sslMode(mode bool) string {
	if mode {
		return "require"
	}
	return "disable"
}
