package db

import (
	"context"
	"database/sql"
	"time"

	_ "modernc.org/sqlite"
)

type SqliteDB struct {
	config *Config
}

func (sqlLite *SqliteDB) Connect() (*sql.DB, error) {
	return sql.Open("sqlite", sqlLite.dsn())
}

func (sqlLite *SqliteDB) Ping() error {
	db, err := sqlLite.Connect()
	if err != nil {
		return err
	}

	defer func(db *sql.DB) {
		_ = db.Close()
	}(db)

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(sqlLite.config.PingTimeout)*time.Second)
	defer cancel()

	return db.PingContext(ctx)
}

func (sqlLite *SqliteDB) dsn() string {
	return sqlLite.config.Name
}
