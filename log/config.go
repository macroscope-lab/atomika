package log

import (
	"gitlab.com/macroscope-lab/atomika/runtime"
)

const (
	TimeFormatRFC       = "rfc3339"
	TimeFormatTimestamp = "timestamp"
)

// Levels ref: https://pkg.go.dev/log/slog#Level
const (
	LevelDebug int = -4
	LevelInfo  int = 0
	LevelWarn  int = 4
	LevelError int = 8
)

const (
	FormatText = "text"
	FormatJSON = "json"
)

const (
	OutputStdOut = 0
	OutputStdErr = 1
)

type Config struct {
	Format     string `mapstructure:"format" envVar:"ATMK_LOG_FORMAT"`
	Level      int    `mapstructure:"level" envVar:"ATMK_LOG_LEVEL"`
	Output     int    `mapstructure:"output" envVar:"ATMK_LOG_OUTPUT"`
	TimeFormat string `mapstructure:"timeFormat" envVar:"ATMK_LOG_TIMEF"`
}

func Configure(key ...string) (*Config, error) {
	c := &Config{}
	if err := runtime.Get(c, key...); err != nil {
		return nil, err
	}
	return c, nil
}

func Defaults() *Config {
	return &Config{
		Format:     FormatText,
		Level:      LevelInfo,
		Output:     OutputStdErr,
		TimeFormat: TimeFormatRFC,
	}
}

func mergeWithDefaults(config *Config) *Config {
	cfg := Defaults()
	_ = runtime.MergeConfigWithDefaults[*Config](config, cfg)
	
	return config
}
