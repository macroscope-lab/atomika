package log

import (
	"context"
	"fmt"
	"io"
	"log/slog"
	"os"
)

func init() {
	sl = newLog(Defaults())
}

var (
	sl *slog.Logger
)

func Logger() *slog.Logger {
	return sl
}

type Ctx struct {
	attrs  []slog.Attr
	logger *slog.Logger
}

func (c Ctx) Debug(msg string, args ...any) {
	c.logger.LogAttrs(context.Background(), slog.LevelDebug, msg, append(c.attrs, slog.Any("args", args))...)
}

func (c Ctx) Info(msg string, args ...any) {
	c.logger.LogAttrs(context.Background(), slog.LevelInfo, msg, append(c.attrs, slog.Any("args", args))...)
}

func (c Ctx) Warn(msg string, args ...any) {
	c.logger.LogAttrs(context.Background(), slog.LevelWarn, msg, append(c.attrs, slog.Any("args", args))...)
}

func (c Ctx) Error(msg string, args ...any) {
	c.logger.LogAttrs(context.Background(), slog.LevelError, msg, append(c.attrs, slog.Any("args", args))...)
}

func (c Ctx) With(kvs ...any) Ctx {
	if len(kvs)%2 != 0 {
		kvs = kvs[:len(kvs)-1] // Discard last element if odd
	}
	newAttrs := make([]slog.Attr, 0, len(c.attrs)+len(kvs)/2)
	newAttrs = append(newAttrs, c.attrs...)

	for i := 0; i < len(kvs); i += 2 {
		key, ok := kvs[i].(string)
		if !ok {
			continue // Handle non-string keys if needed
		}
		newAttrs = append(newAttrs, slog.String(key, fmt.Sprintf("%v", kvs[i+1])))
	}

	return Ctx{
		attrs:  newAttrs,
		logger: slog.New(sl.Handler()),
	}
}

func New(cfg *Config) {
	sl = newLog(cfg)
}

func With(kvs ...any) Ctx {
	if len(kvs)%2 != 0 {
		kvs = kvs[:len(kvs)-1] // Discard last element if odd
	}

	newAttrs := make([]slog.Attr, 0, len(kvs)/2)

	for i := 0; i < len(kvs); i += 2 {
		key, ok := kvs[i].(string)
		if !ok {
			continue // Handle non-string keys if needed
		}
		newAttrs = append(newAttrs, slog.String(key, fmt.Sprintf("%v", kvs[i+1])))
	}

	return Ctx{
		attrs:  newAttrs,
		logger: slog.New(sl.Handler()),
	}
}

func newLog(cfg *Config) *slog.Logger {
	if cfg == nil {
		cfg = &Config{}
	}
	mergeWithDefaults(cfg)

	opts := parseSlogOpts(cfg)

	var writer io.Writer = os.Stderr
	if cfg.Output == OutputStdOut {
		writer = os.Stdout
	}

	var handler slog.Handler = slog.NewTextHandler(writer, opts)
	if cfg.Format == "json" {
		handler = slog.NewJSONHandler(writer, opts)
	}

	return slog.New(handler)
}

func parseSlogOpts(cfg *Config) *slog.HandlerOptions {
	opts := &slog.HandlerOptions{
		Level: slog.Level(cfg.Level),
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
			// Handle time format
			if a.Key == slog.TimeKey {
				if cfg.TimeFormat == "timestamp" {
					t := a.Value.Time()
					a.Value = slog.Int64Value(t.UnixNano() / 1000)
				}
			}
			return a
		},
	}
	return opts
}
