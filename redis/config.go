package redis

import (
	"gitlab.com/macroscope-lab/atomika/runtime"
)

type Config struct {
	Addrs []string `mapstructure:"redisAddrs" envVar:"ATMK_REDIS_ADDRS"`
	Auth  string   `mapstructure:"redisAuth" envVar:"ATMK_REDIS_AUTH"`
	DB    int      `mapstructure:"redisDb" envVar:"ATMK_REDIS_DB"`
}

func Configure(key ...string) (*Config, error) {
	c := &Config{}
	if err := runtime.Get(c, key...); err != nil {
		return nil, err
	}
	return c, nil
}

func Defaults() *Config {
	return &Config{
		Addrs: []string{
			"0.0.0.0:6379",
		},
		DB: 0,
	}
}

func mergeWithDefaults(config *Config) *Config {
	cfg := Defaults()
	_ = runtime.MergeConfigWithDefaults[*Config](config, cfg)
	
	return config
}
