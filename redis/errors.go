package redis

import (
	"errors"
	"fmt"
	"strings"
)

var (
	ErrConn               = errors.New("cannot connect to address")
	ErrShutdown           = errors.New("cannot shutdown redisCluster client")
	ErrZSetEntryAdd       = errors.New("cannot add entry to zset")
	ErrZSetEntryDelete    = errors.New("cannot delete entry from zset")
	ErrStreamNotExist     = errors.New("ERR no such key")
	ErrConsumerGroupExist = errors.New("BUSYGROUP Consumer Group name already exists")
)

var (
	pkgListenBindErr = `connect: connection refused`
)

func checkError(err error) error {
	switch {
	case strings.Contains(err.Error(), pkgListenBindErr):
		return fmt.Errorf(ErrConn.Error()+":[%s]", err)
	default:
		return err
	}
}
