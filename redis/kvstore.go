package redis

import (
	"context"
	"time"
	
	"gitlab.com/macroscope-lab/atomika/log"
)

type (
	KVStore interface {
		Set(ctx context.Context, key string, value interface{}, expiry time.Duration) error
		Get(ctx context.Context, key string) (string, error)
		Delete(ctx context.Context, key string) error
	}
	
	kvStore struct {
		client Redis
		log    log.Ctx
	}
)

// Set sets a key to hold the specified value. If expiry is non-zero, the key will expire after the given duration.
func (kv kvStore) Set(ctx context.Context, key string, value interface{}, expiry time.Duration) error {
	
	return kv.client.Client().Set(ctx, key, value, expiry).Err()
}

// Get retrieves the value of a key. If the key does not exist, an empty string is returned.
func (kv kvStore) Get(ctx context.Context, key string) (string, error) {
	return kv.client.Client().Get(ctx, key).Result()
}

// Delete removes the specified key. A key is ignored if it does not exist.
func (kv kvStore) Delete(ctx context.Context, key string) error {
	return kv.client.Client().Del(ctx, key).Err()
}

// NewKVStore creates a new instance of kvStore.
func NewKVStore(client Redis) KVStore {
	return kvStore{
		client: client,
		log:    log.With("module", "kvStore"),
	}
}
