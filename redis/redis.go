package redis

import (
	"context"
	"time"
	
	goRedisV9 "github.com/redis/go-redis/v9"
	"gitlab.com/macroscope-lab/atomika"
	"gitlab.com/macroscope-lab/atomika/log"
)

// TODO: Refactor this file to match both cluster and single client

// CmdableExtension interface extension to pass to various Redis
// extensions  in this package
type CmdableExtension interface {
	goRedisV9.Cmdable
}

type Redis interface {
	atomika.Service
	Client() CmdableExtension
	Log() log.Ctx
	Close() error
}

// redisClient create a redis instance with simple client
type redisClient struct {
	log log.Ctx
	// options Options
	client *goRedisV9.Client
}

func (service *redisClient) Run(ctx context.Context) error {
	service.log.Info("service started")
	
	errChan := make(chan error, 1)
	
	// Check connections
	// NOTE! This will error even if one node in the cluster is offline
	// but will not cause the service to crash
	if err := service.client.Ping(ctx).Err(); err != nil {
		errChan <- err
	}
	
	for {
		select {
		case <-ctx.Done():
			return shutdown(context.Background(), service)
		case err := <-errChan:
			err = checkError(err)
			service.log.Error(err.Error(), "status", "faulty")
			return nil
		}
	}
}

func (service *redisClient) Log() log.Ctx {
	return service.log
}

func (service *redisClient) Close() error {
	return service.client.Close()
}

// Client allows access to the underlying redisClient client library
func (service *redisClient) Client() CmdableExtension {
	return service.client
}

// redisCluster create a connection with redis cluster
type redisCluster struct {
	log log.Ctx
	// options Options
	client *goRedisV9.ClusterClient
}

func (service *redisCluster) Run(ctx context.Context) error {
	service.log.Info("service started")
	
	errChan := make(chan error, 1)
	
	// Check connections
	// NOTE! This will error even if one node in the cluster is offline
	// but will not cause the service to crash
	if err := service.client.Ping(context.TODO()).Err(); err != nil {
		errChan <- err
	}
	
	for {
		select {
		case <-ctx.Done():
			return shutdown(context.Background(), service)
		case err := <-errChan:
			err = checkError(err)
			service.log.Error(err.Error(), "status", "faulty")
			return nil
		}
	}
}

func (service *redisCluster) Log() log.Ctx {
	return service.log
}

func (service *redisCluster) Close() error {
	return service.client.Close()
}

// Client allows access to the underlying redisCluster client library
func (service *redisCluster) Client() CmdableExtension {
	return service.client
}

// New create a new Redis client/cluster connection handler
func New(cfg *Config) (Redis, error) {
	var err error
	if cfg == nil {
		cfg, err = Configure()
	}
	if err != nil {
		return nil, err
	}
	
	mergeWithDefaults(cfg)
	
	if len(cfg.Addrs) > 1 {
		return newRedisClusterClient(cfg)
	}
	
	return newRedisClient(cfg)
}

func newRedisClient(cfg *Config) (*redisClient, error) {
	
	client := goRedisV9.NewClient(&goRedisV9.Options{
		Addr:     cfg.Addrs[0],
		Password: cfg.Auth,
		DB:       cfg.DB,
	})
	
	lg := log.With("service", "redisClient")
	
	cl := &redisClient{
		log:    lg,
		client: client,
	}
	
	return cl, nil
}

func newRedisClusterClient(cfg *Config) (*redisCluster, error) {
	
	client := goRedisV9.NewClusterClient(&goRedisV9.ClusterOptions{
		Addrs:    cfg.Addrs,
		Password: cfg.Auth,
		NewClient: func(opt *goRedisV9.Options) *goRedisV9.Client {
			opt.DB = cfg.DB
			return goRedisV9.NewClient(opt)
		},
		RouteRandomly: true,
	})
	
	lg := log.With("service", "redisCluster")
	
	clusterClient := &redisCluster{
		client: client,
		log:    lg,
	}
	
	return clusterClient, nil
}

func shutdown(ctx context.Context, client Redis) error {
	_, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()
	
	if err := client.Close(); err != nil {
		return ErrShutdown
	}
	
	client.Log().Info("redis shutdown")
	return nil
}
