package redis

import (
	"context"
	"fmt"
	"sync"
	"time"
	
	goRedisV9 "github.com/redis/go-redis/v9"
	"gitlab.com/macroscope-lab/atomika/log"
)

type (
	SortedSet interface {
		C() <-chan Entry
		Start(ctx context.Context, interval time.Duration)
		Stop()
		AddEntry(ctx context.Context, entry Entry) error
		DeleteEntry(ctx context.Context, entryID string) error
		GetEntries(ctx context.Context, min, max string) ([]Entry, error)
	}
	
	Entry struct {
		ID        string
		ExecuteAt time.Time
	}
	
	zSortedSet struct {
		client   Redis
		c        chan Entry
		log      log.Ctx
		name     string
		ticker   *time.Ticker
		stopChan chan struct{}
		wg       sync.WaitGroup
	}
)

// Start begins the job processing loop.
func (s *zSortedSet) Start(ctx context.Context, interval time.Duration) {
	s.ticker = time.NewTicker(interval)
	s.c = make(chan Entry)
	s.log.Debug("create ticker", "interval", interval.String())
	
	go s.startProcess(ctx)
}

// Stop stops the job processing loop.
func (s *zSortedSet) Stop() {
	close(s.stopChan)
	close(s.c)
	
	s.ticker.Stop()
	
	s.wg.Wait()
	
	s.log.Debug("ticker stopped")
}

func (s *zSortedSet) AddEntry(ctx context.Context, entry Entry) error {
	score := float64(entry.ExecuteAt.Unix())
	err := s.client.Client().ZAdd(ctx, s.name, goRedisV9.Z{
		Score:  score,
		Member: entry.ID,
	}).Err()
	
	if err != nil {
		return fmt.Errorf("%w:[%s]", ErrZSetEntryAdd, err.Error())
	}
	
	return nil
}

func (s *zSortedSet) DeleteEntry(ctx context.Context, entryID string) error {
	return s.client.Client().ZRem(ctx, s.name, entryID).Err()
}

func (s *zSortedSet) GetEntries(ctx context.Context, min, max string) ([]Entry, error) {
	zEntries, err := s.processEntries(ctx, min, max)
	if err != nil {
		return nil, err
	}
	
	entries := make([]Entry, len(zEntries))
	for i, entry := range zEntries {
		s.log.Debug("processing entry", "entryID", entry.Member, "method", "GetEntries")
		
		// gocritic:disable
		id := entry.Member.(string)
		executedAt := time.Unix(floatToTimestamp(entry.Score))
		
		entries[i] = Entry{
			ID:        id,
			ExecuteAt: executedAt,
		}
	}
	
	return entries, nil
}

func (s *zSortedSet) C() <-chan Entry {
	return s.c
}

// startProcess periodically check the entries in the stack
func (s *zSortedSet) startProcess(ctx context.Context) {
	s.wg.Add(1)
	defer s.wg.Done()
	
	for {
		select {
		case <-s.ticker.C:
			s.processEntriesOnChan(ctx)
		case <-s.stopChan:
			s.log.Debug("stopping ticker")
			return
		}
	}
}

func (s *zSortedSet) processEntriesOnChan(ctx context.Context) {
	// Always set time.Now here to move stack max limit
	now := time.Now().UTC().Unix()
	
	zEntries, err := s.processEntries(ctx, "0", fmt.Sprintf("%d", now))
	if err != nil {
		s.log.Error(err.Error())
		s.c <- Entry{}
	}
	
	for _, entry := range zEntries {
		s.log.Debug("processing entry", "entryID", entry.Member, "method", "processEntriesOnChan")
		
		// gocritic:disable
		id := entry.Member.(string)
		executedAt := time.Unix(floatToTimestamp(entry.Score))
		
		s.c <- Entry{
			ID:        id,
			ExecuteAt: executedAt,
		}
	}
}

// processEntries processes jobs that are due.
func (s *zSortedSet) processEntries(ctx context.Context, min, max string) ([]goRedisV9.Z, error) {
	zEntries, err := s.client.Client().ZRangeByScoreWithScores(ctx, s.name, &goRedisV9.ZRangeBy{
		Min: min,
		Max: max,
	}).Result()
	if err != nil {
		s.log.Error(err.Error())
		return nil, err
	}
	
	return zEntries, nil
	
}

// NewZSortedSet start a new ZSet internal ticker with a name and a ticker in seconds
func NewZSortedSet(client Redis, name string) SortedSet {
	return &zSortedSet{
		client:   client,
		log:      log.With("module", "zSortedSet"),
		name:     name,
		stopChan: make(chan struct{}),
	}
}

func floatToTimestamp(floatTs float64) (int64, int64) {
	sec := int64(floatTs)
	nanoSec := int64((floatTs - float64(sec)) * 1e9)
	
	return sec, nanoSec
}
