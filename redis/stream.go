package redis

import (
	"context"
	"fmt"
	"time"
	
	goRedisV9 "github.com/redis/go-redis/v9"
	"gitlab.com/macroscope-lab/atomika/log"
)

type (
	Streamer interface {
		CreateStream(ctx context.Context, streamName, groupName string) error
		CleanupStream(ctx context.Context, streamName string, retentionPeriod time.Duration)
		Publish(ctx context.Context, streamName string, message map[string]interface{}) error
		Subscribe(ctx context.Context, streamName, groupName, consumerName string, handler StreamMessageHandler) error
	}
	
	xStream struct {
		client Redis
		log    log.Ctx
	}
)

func (x *xStream) CreateStream(ctx context.Context, streamName, groupName string) error {
	if err := x.streamInfo(ctx, streamName); err != nil {
		if err.Error() != ErrStreamNotExist.Error() {
			return err
		}
	}
	
	_, err := x.client.Client().XGroupCreateMkStream(ctx, streamName, groupName, "0").Result()
	if err != nil {
		if err.Error() == ErrConsumerGroupExist.Error() {
			return nil
		}
		
		return err
	}
	
	return nil
}

func (x *xStream) CleanupStream(ctx context.Context, streamName string, retentionPeriod time.Duration) {
	now := time.Now()
	oldestTimestamp := now.Add(-retentionPeriod).UnixMilli()
	ticker := time.NewTicker(5 * time.Second)
	for {
		select {
		case <-ctx.Done():
		case <-ticker.C:
			id := fmt.Sprintf("%d-0", oldestTimestamp)
			fmt.Printf("Cleanup stream %s starting from ID %s\n", streamName, id)
		}
	}
}

func (x *xStream) Publish(ctx context.Context, streamName string, message map[string]interface{}) error {
	err := x.client.Client().XAdd(ctx, &goRedisV9.XAddArgs{
		Stream:     streamName,
		NoMkStream: true,
		ID:         "*",
		Values:     message,
	}).Err()
	
	if err != nil {
		return err
	}
	
	// Store data worth of one year on each stream with approximation
	yearToSecs := int64(365 * 24 * 60 * 60)
	x.client.Client().XTrimMaxLenApprox(ctx, streamName, yearToSecs, 0)
	
	return nil
}

func (x *xStream) Subscribe(ctx context.Context, streamName, groupName, consumerName string, handler StreamMessageHandler) error {
	
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
			messages, err := x.client.Client().XReadGroup(ctx, &goRedisV9.XReadGroupArgs{
				Group:    groupName,
				Consumer: fmt.Sprintf("%s-%s", groupName, consumerName),
				Streams:  []string{streamName, ">"},
				// Read one message at a time
				Count: 1,
				// Do not wait and read as soon as new message is available
				Block: 0,
				// Do not Auto ACK message allow subscriber to do it
				NoAck: false,
			}).Result()
			
			if err != nil {
				return err
			}
			
			if err = x.processMessagesOnStream(ctx, groupName, messages, handler); err != nil {
				return err
			}
		}
	}
}

func (x *xStream) streamInfo(ctx context.Context, streamName string) error {
	_, err := x.client.Client().XInfoStream(ctx, streamName).Result()
	if err != nil {
		return err
	}
	
	return nil
}

func (x *xStream) processMessagesOnStream(ctx context.Context, groupName string, messages []goRedisV9.XStream, handler StreamMessageHandler) error {
	for _, message := range messages {
		for _, messageItem := range message.Messages {
			
			req := StreamMessageReq{
				ID:     messageItem.ID,
				Values: messageItem.Values,
				Stream: message.Stream,
			}
			
			res := handler.HandleMessage(ctx, req)
			if res.Processed {
				_, err := x.client.Client().XAck(ctx, message.Stream, groupName, messageItem.ID).Result()
				if err != nil {
					return err
				}
			}
			
			if res.Error != nil {
				return res.Error
			}
		}
	}
	return nil
}

// NewStreamer create a new streamer instance
func NewStreamer(client Redis) Streamer {
	return &xStream{
		client: client,
		log:    log.With("module", "xStream"),
	}
}
