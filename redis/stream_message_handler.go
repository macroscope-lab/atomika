package redis

import (
	"context"

	"github.com/mitchellh/mapstructure"
)

type StreamMessageHandler interface {
	HandleMessage(ctx context.Context, req StreamMessageReq) StreamMessageRes
}

type StreamMessageReq struct {
	ID     string
	Stream string
	Values map[string]interface{}
}

func (msg *StreamMessageReq) Decode(v interface{}) error {
	return mapstructure.Decode(msg.Values, v)
}

type StreamMessageRes struct {
	Processed bool
	Error     error
}
