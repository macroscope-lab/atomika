package runtime

import (
	"os"
	"reflect"
	"strconv"
	"strings"

	"github.com/spf13/viper"
)

const (
	runtimeTag = "envVar"
)

type Validatable interface {
	Validate() error
}

type Configurable interface {
	Configure(key ...string) error
}

// type DefaultsApplier interface {
// 	Defaults()
// }

func Get(v any, key ...string) error {
	if reflect.ValueOf(v).Kind() != reflect.Ptr {
		return errNotAPointer
	}

	// v = withDefaultValues(v)

	// Search in config file from a specific key
	if len(key) > 0 && key[0] != "" {
		// Search in json specified key then default ev vars
		if err := viper.UnmarshalKey(key[0], v); err != nil {
			return err
		}
	} else {
		// Search in env vars and default locations
		if err := viper.Unmarshal(v); err != nil {
			return err
		}
	}

	// Env vars have priority, so we overwrite with any found
	// value with an env var is set
	if err := bindKeyToEnv(v); err != nil {
		return err
	}

	if err := withValidation(v); err != nil {
		return err
	}

	return nil
}

func MergeConfigWithDefaults[T any](cfgDest, defaultCfgDest T) error {
	dstVal := reflect.ValueOf(cfgDest).Elem()
	defaultVal := reflect.ValueOf(defaultCfgDest).Elem()

	for i := 0; i < dstVal.NumField(); i++ {
		dstField := dstVal.Field(i)
		defaultField := defaultVal.Field(i)

		if isZeroValues(dstField) {
			setField(dstField, defaultField)
		}
	}

	return nil
}

func isZeroValues(field reflect.Value) bool {
	switch field.Kind() {
	case reflect.String:
		return field.Len() == 0
	case reflect.Ptr:
		return field.IsNil()
	default:
		return reflect.DeepEqual(field.Interface(), reflect.Zero(field.Type()).Interface())
	}
}

func setField(target, source reflect.Value) {
	if target.CanSet() {
		if source.Kind() == reflect.Ptr && source.IsNil() {
			return
		}
		// Dereference pointers if necessary
		if target.Kind() == reflect.Ptr && !target.IsNil() {
			target.Elem().Set(source)
		} else {
			target.Set(source)
		}
	}
}

func bindKeyToEnv(v interface{}) error {
	// Get the value of the struct
	val := reflect.ValueOf(v)

	// Check if the value is a pointer and dereference it if necessary
	if val.Kind() != reflect.Ptr {
		return errNotAPointer
	}
	val = val.Elem()

	// Iterate over the struct fields
	for i := 0; i < val.NumField(); i++ {
		field := val.Field(i)
		fieldType := field.Type()
		envName := val.Type().Field(i).Tag.Get(runtimeTag)

		switch field.Kind() {
		case reflect.String:
			if envValue := os.Getenv(envName); envValue != "" {
				field.SetString(envValue)
			}
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			if envValue := os.Getenv(envName); envValue != "" {
				envInt, _ := strconv.ParseInt(envValue, 10, 64)
				field.SetInt(envInt)
			}
		case reflect.Bool:
			if envValue := os.Getenv(envName); envValue != "" {
				envBool, _ := strconv.ParseBool(envValue)
				field.SetBool(envBool)
			}
		case reflect.Slice:
			if envValue := os.Getenv(envName); envValue != "" {
				// Split the environment variable value by commas and set the slice field
				sliceValues := strings.Split(envValue, ",")
				for i := range sliceValues {
					sliceValues[i] = strings.TrimSpace(sliceValues[i])
				}

				slice := reflect.MakeSlice(fieldType, len(sliceValues), len(sliceValues))
				for j, v := range sliceValues {
					slice.Index(j).SetString(v)
				}
				field.Set(slice)
			}
		}

	}

	return nil
}

// func withDefaultValues(v interface{}) interface{} {
// 	val, ok := v.(DefaultsApplier)
// 	if ok {
// 		val.Defaults()
// 		v = val
// 	}
// 	return v
// }

func withValidation(v any) error {
	val, ok := v.(Validatable)
	if ok {
		return val.Validate()
	}

	return nil
}
