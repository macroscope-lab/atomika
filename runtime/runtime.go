package runtime

import (
	"errors"
	"log"
	"os"
	"path/filepath"

	"github.com/spf13/viper"
)

var (
	ignoredFolders = []string{".git", "tmp", "vendor"}
)

var (
	errProjectConfig = errors.New("unable to load project config files")
	errNotAPointer   = errors.New("value is not a pointer")
)

func init() {

	if err := setupAtomika(); err != nil {
		log.Fatalln(errProjectConfig, err)
	}
}

func setupAtomika() error {
	wd, err := os.Getwd()
	if err != nil {
		return err
	}

	viper.AddConfigPath(wd)
	_ = viper.ReadInConfig()

	err = filepath.Walk(wd, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			for _, ignored := range ignoredFolders {
				if ignored == info.Name() {
					return filepath.SkipDir
				}
			}
		}
		var fPath string
		if filepath.Ext(path) == ".json" || filepath.Ext(path) == ".JSON" {
			fPath, err = filepath.Abs(path)
			if err != nil {
				return err
			}

			viper.SetConfigFile(fPath)

			if err = viper.MergeInConfig(); err != nil {
				return err
			}
		}

		return nil
	})
	if err != nil {
		return err
	}

	return nil
}
