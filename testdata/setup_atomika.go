package testdata

import (
	_ "unsafe"
)

var workspaceJSON = `{
 "id": "test-x-vrpksr",
 "repo": "github.com",
 "services": [
  {
   "id": "user-management-jnnizb",
   "path": "user-management",
   "version": "0.0.1"
  },
  {
   "id": "comments-sbuczu",
   "path": "comments",
   "version": "0.0.1"
  }
 ]
}`

// This `setupAtomika` is bare minimum

//go:linkname setupAtomika gitlab.com/macroscope-lab/atomika/runtime.setupAtomika
func setupAtomika() ([][]byte, error) {

	var data [][]byte

	// Append WORKSPACE information first
	data = append(data, []byte(workspaceJSON))
	return data, nil
}
