package transport

import (
	"gitlab.com/macroscope-lab/atomika/runtime"
)

type Certs struct {
}

type Config struct {
	BasePath string `mapstructure:"basePath" envVar:"ATMK_TRANSPORT_BASEPATH"`
	Name     string `mapstructure:"name" envVar:"ATMK_TRANSPORT_NAME"`
	Port     string `mapstructure:"port" envVar:"ATMK_TRANSPORT_PORT"`
	WWW      string `mapstructure:"www" envVar:"ATMK_TRANSPORT_WWW"`
	
	// CORS
	AllowedOrigins   []string `mapstructure:"allowedOrigins" envVar:"ATMK_CORS_ORIGINS"`
	AllowedMethods   []string `mapstructure:"allowedMethods" envVar:"ATMK_CORS_METHODS"`
	AllowedHeaders   []string `mapstructure:"allowedHeaders" envVar:"ATMK_CORS_HEADERS"`
	AllowCredentials bool     `mapstructure:"allowCredentials" envVar:"ATMK_CORS_CREDS"`
	
	// TLS
	TlsCa   string `mapstructure:"tlsCa" envVar:"ATMK_TLS_CA"`
	TlsCert string `mapstructure:"tlsCert" envVar:"ATMK_TLS_CERT"`
	TlsKey  string `mapstructure:"tlsKey" envVar:"ATMK_TLS_KEY"`
}

func Configure(key ...string) (*Config, error) {
	c := &Config{}
	if err := runtime.Get(c, key...); err != nil {
		return nil, err
	}
	return c, nil
}

func Defaults() *Config {
	return &Config{
		BasePath: "/rpc/",
		Port:     "8080",
	}
}

func mergeWithDefaults(config *Config) *Config {
	cfg := Defaults()
	_ = runtime.MergeConfigWithDefaults[*Config](config, cfg)
	
	return config
}
