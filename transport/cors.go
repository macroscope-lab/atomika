package transport

import (
	"net/http"

	rsCors "github.com/rs/cors"
)

type corsHandler struct {
	cfg *Config
}

func (c corsHandler) Handle(h http.Handler) http.Handler {
	co := defaultCORSOptions()

	co.AllowCredentials = c.cfg.AllowCredentials

	if len(c.cfg.AllowedHeaders) > 0 {
		co.AllowedHeaders = c.cfg.AllowedHeaders
	}

	if len(c.cfg.AllowedOrigins) > 0 {
		co.AllowedOrigins = c.cfg.AllowedOrigins
	}

	if len(c.cfg.AllowedMethods) > 0 {
		co.AllowedMethods = c.cfg.AllowedMethods
	}

	return rsCors.New(co).Handler(h)
}

func defaultCORSOptions() rsCors.Options {
	return rsCors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{
			http.MethodHead,
			http.MethodOptions,
			http.MethodGet,
			http.MethodPost,
		},
		AllowedHeaders: []string{"*"},
	}
}
