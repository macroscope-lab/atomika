package transport

import (
	"errors"
	"strings"
)

var (
	ErrBadPayload       = errors.New("unable to decode payload")
	ErrUnmarshalPayload = errors.New("unable to unmarshal payload")
	ErrPathNotFound     = errors.New("path not found")
	ErrAddrInUse        = errors.New("address:port already in use")
)

type Error struct {
	Code Code        `json:"code"`
	Msg  interface{} `json:"msg"`
}

func (e *Error) Error() string {
	switch msg := e.Msg.(type) {
	case string:
		return msg
	case error:
		return msg.Error()
	default:
		return "Unknown error"
	}
}

var (
	listenPattern = `bind: address already in use`
)

func checkError(err error) error {
	switch {
	case strings.Contains(err.Error(), listenPattern):
		return ErrAddrInUse
	default:
		return err
	}

}
