package transport

import (
	"errors"
	"github.com/stretchr/testify/suite"
	"testing"
)

// Define a test suite for CustomError.
type ErrorSuite struct {
	suite.Suite
}

func (suite *ErrorSuite) TestCustomErrorStringMsg() {
	// Test with a CustomError instance containing a string message.
	err := &Error{
		Msg:  "This is a custom error message",
		Code: OK,
	}

	// Assert that the error message matches the expected string message.
	suite.Equal("This is a custom error message", err.Error())
}

func (suite *ErrorSuite) TestCustomErrorErrorMsg() {
	// Test with a CustomError instance containing an error message.
	err := &Error{
		Msg:  errors.New("this is an error"),
		Code: Unknown,
	}

	// Assert that the error message matches the expected error message.
	suite.Equal("this is an error", err.Error())
}

func TestErrorSuite(t *testing.T) {
	suite.Run(t, new(ErrorSuite))
}
