package transport

import (
	"net/http"
)

const GlobalUse = "*"

// Interceptor definition
type Interceptor interface {
	Intercept(http.HandlerFunc) http.HandlerFunc
}

func Intercept(target http.HandlerFunc, interceptors ...Interceptor) http.HandlerFunc {
	if len(interceptors) == 0 {
		return target
	}

	next := target

	// loop in reverse to preserve middleware order
	for i := len(interceptors) - 1; i >= 0; i-- {
		next = interceptors[i].Intercept(next)
	}

	return next

}

// interceptorsStore interceptors for global or service/action use
type interceptorsStore struct {
	store map[string][]Interceptor
}

func newInterceptorsStore() *interceptorsStore {
	return &interceptorsStore{
		store: make(map[string][]Interceptor),
	}
}

// Add interceptors to a specific route
func (is *interceptorsStore) Add(route string, iList []Interceptor) {
	if _, ok := is.store[route]; !ok {
		is.store[route] = append(is.store[route], iList...)
	}
}

// Global get all global interceptors
func (is *interceptorsStore) Global() []Interceptor {
	return is.store[GlobalUse]
}

// Get routers for a specific route
func (is *interceptorsStore) Get(route string) []Interceptor {
	var iList []Interceptor
	if rInterceptors, ok := is.store[route]; ok {
		iList = append(iList, rInterceptors...)
	}

	return iList
}
