package transport

import (
	"net/http"
)

type Route struct {
	ServiceName string
	ActionName  string
	Handler     http.HandlerFunc
	Method      string
}
