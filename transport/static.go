package transport

import (
	"net/http"
)

type emptyHandler struct {
}

func (s emptyHandler) ServeHTTP(w http.ResponseWriter, _ *http.Request) {
	_, _ = w.Write([]byte("No public interface"))
}

type staticACK struct {
}

func (s staticACK) ServeHTTP(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
}
