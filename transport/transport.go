package transport

import (
	"compress/gzip"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"path"
	"strings"
	"time"
	
	"gitlab.com/macroscope-lab/atomika"
	"gitlab.com/macroscope-lab/atomika/log"
	"gitlab.com/macroscope-lab/atomika/types"
)

const (
	msgWarnRouteExist = "route already exist. Skipping!"
)

type Transport interface {
	atomika.Service
	OnError(w http.ResponseWriter, r *http.Request, err error)
	Register(route Route)
	ServeHTTP(w http.ResponseWriter, r *http.Request)
	Use(route string, interceptorsList []Interceptor)
	UI(ui *UI)
}

type transport struct {
	config           *Config
	log              log.Ctx
	client           *http.Server
	ui               *UI
	routes           map[string]Route
	interceptorsList *interceptorsStore
	
	notFound http.HandlerFunc
	onError  func(w http.ResponseWriter, r *http.Request, err error)
}

func (service *transport) Run(ctx context.Context) error {
	errChan := make(chan error, 1)
	
	mux := http.NewServeMux()
	mux.Handle("/", service.ui.Handler())
	mux.Handle(service.config.BasePath, service)
	
	addr := fmt.Sprintf("[::]:%s", service.config.Port)
	cors := corsHandler{
		cfg: service.config,
	}
	
	{
		service.client.Addr = addr
		service.client.Handler = cors.Handle(mux)
	}
	
	go func(server *http.Server, cfg *Config) {
		switch {
		case cfg.TlsCert != "" && cfg.TlsKey != "":
			if err := server.ListenAndServeTLS(cfg.TlsCert, cfg.TlsKey); !errors.Is(err, http.ErrServerClosed) {
				errChan <- err
			}
		default:
			if err := server.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
				errChan <- err
			}
		}
	}(service.client, service.config)
	
	service.log.Info("service started", "port", addr)
	
	for {
		select {
		case <-ctx.Done():
			return shutdown(context.Background(), service)
		case err := <-errChan:
			err = checkError(err)
			service.log.Error(err.Error())
			return nil
		}
	}
}

func (service *transport) OnError(w http.ResponseWriter, r *http.Request, err error) {
	service.onError(w, r, err)
}

func (service *transport) Register(route Route) {
	routePath := path.Clean(fmt.Sprintf("%s%s/%s", service.config.BasePath, route.ServiceName, route.ActionName))
	
	if _, ok := service.routes[routePath]; ok {
		service.log.Error(msgWarnRouteExist, "route", route)
		return
	}
	
	// Get global interceptors
	iList := service.interceptorsList.Global()
	
	// Get service interceptors
	iList = append(iList, service.interceptorsList.Get(path.Join(route.ServiceName, GlobalUse))...)
	
	// Get service method interceptors
	iList = append(iList, service.interceptorsList.Get(path.Join(route.ServiceName, route.ActionName))...)
	
	route.Handler = Intercept(route.Handler, iList...)
	
	service.routes[routePath] = route
}

func (service *transport) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	
	// Add Tracing KEY
	key := types.GenUUIDv4()
	ctx := context.WithValue(r.Context(), types.CtxTraceKey, key.String())
	r = r.WithContext(ctx)
	
	// Release the response right away in these cases
	if r.RequestURI == "*" && r.Method == "OPTIONS" {
		ack := &staticACK{}
		ack.ServeHTTP(w, r)
	}
	
	// Get Route
	h, ok := service.routes[r.URL.Path]
	if !ok {
		service.notFound.ServeHTTP(w, r)
		return
	}
	
	h.Handler.ServeHTTP(w, r)
}

func (service *transport) Use(route string, interceptorsList []Interceptor) {
	
	switch route {
	case GlobalUse, "":
		route = GlobalUse
	default:
		rArr := strings.Split(route, "/")
		if len(rArr) < 2 {
			route = path.Join(rArr[0], GlobalUse)
		}
	}
	
	service.interceptorsList.Add(route, interceptorsList)
}

// UI attach a custom UI location for the service
func (service *transport) UI(ui *UI) {
	service.ui = ui
}

// Decode incoming http request body
func Decode(r io.ReadCloser, data any) error {
	bodyBytes, err := io.ReadAll(io.LimitReader(r, 1024*1024))
	if err != nil {
		return fmt.Errorf("%w", ErrBadPayload)
	}
	
	err = json.Unmarshal(bodyBytes, data)
	if err != nil {
		return fmt.Errorf("%w", ErrUnmarshalPayload)
	}
	return nil
}

// Encode outgoing http response
func Encode(w http.ResponseWriter, r *http.Request, status int, data any) error {
	var writer io.Writer = w
	
	b, err := json.Marshal(data)
	if err != nil {
		return err
	}
	
	if strings.Contains(r.Header.Get("Accept-Encoding"), "gzip") {
		w.Header().Set("Content-Encoding", "gzip")
		gzw := gzip.NewWriter(w)
		writer = gzw
		defer func(gzw *gzip.Writer) {
			_ = gzw.Close()
		}(gzw)
	}
	
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(status)
	
	if _, err = writer.Write(b); err != nil {
		return err
	}
	
	return nil
}

func New(cfg *Config) (Transport, error) {
	return newTransport(cfg)
}

func newTransport(cfg *Config) (*transport, error) {
	
	var err error
	if cfg == nil {
		cfg, err = Configure()
	}
	if err != nil {
		return nil, err
	}
	
	mergeWithDefaults(cfg)
	
	lg := log.With("service", "transport")
	
	client := &http.Server{}
	ui := NewWithEmptyHandler()
	
	if cfg.WWW != "" {
		ui = &UI{
			Dir: cfg.WWW,
		}
	}
	
	return &transport{
		config:           cfg,
		log:              lg,
		client:           client,
		routes:           make(map[string]Route),
		interceptorsList: newInterceptorsStore(),
		notFound:         onNotFoundHandler,
		onError:          onErrorHandler,
		ui:               ui,
	}, nil
}

func onErrorHandler(w http.ResponseWriter, r *http.Request, err error) {
	_ = Encode(w, r, http.StatusInternalServerError, Error{
		Internal,
		err.Error(),
	})
}

func onNotFoundHandler(w http.ResponseWriter, r *http.Request) {
	_ = Encode(w, r, http.StatusNotFound, Error{
		NotFound,
		ErrPathNotFound.Error(),
	})
}

func shutdown(ctx context.Context, client *transport) error {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()
	
	if err := client.client.Shutdown(ctx); err != nil {
		return err
	}
	client.log.Info("transport shutdown")
	return nil
}
