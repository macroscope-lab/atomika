package transport

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	
	"github.com/stretchr/testify/require"
)

type mockReq struct {
	Name string
}

func Test_Register(t *testing.T) {
	srv := &transport{
		config: &Config{
			BasePath: "/rpc/",
		},
		routes:           make(map[string]Route),
		notFound:         onNotFoundHandler,
		onError:          onErrorHandler,
		interceptorsList: newInterceptorsStore(),
	}
	
	r := Route{
		ServiceName: "TestService",
		ActionName:  "Test",
		Handler:     nil,
		Method:      "POST",
	}
	srv.Register(r)
	
	_, ok := srv.routes["/rpc/TestService/Test"]
	require.Equal(t, true, ok)
}

func TestDecode(t *testing.T) {
	data := `
		{"name": "Atomika"}
	`
	
	req, err := http.NewRequest(http.MethodPost, "/rpc/MockService/MockMethod", strings.NewReader(data))
	
	require.NoError(t, err)
	
	req.Header.Set("Content-Type", "application/json")
	
	dto := &mockReq{}
	err = Decode(req.Body, dto)
	
	require.NoError(t, err)
	require.Equal(t, dto.Name, "Atomika")
}

func TestEncode(t *testing.T) {
	tests := []struct {
		name string
		args interface{}
		want interface{}
	}{
		{
			name: "OK",
			want: OK,
			args: struct {
				Name string `json:"name,omitempty"`
			}{
				Name: "Test",
			},
		},
		{
			name: "Errored",
			want: Unknown,
			args: errors.New("some-error"),
		},
	}
	
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := httptest.NewRecorder()
			r := httptest.NewRequest(http.MethodPost, "/rpc/MockService/MockMethod", strings.NewReader(`-`))
			
			enc := Encode(w, r, http.StatusOK, tt.args)
			
			require.NoError(t, enc)
			require.Equal(t, http.StatusOK, w.Code)
			require.Equal(t, "application/json; charset=utf-8", w.Header().Get("Content-Type"))
		})
	}
}

func TestNew(t *testing.T) {
	refTransport, _ := New(Defaults())
	require.Implements(t, (*Transport)(nil), refTransport)
}

func TestTransport_ServeHTTP(t *testing.T) {
	
	type args struct {
		method  string
		path    string
		payload interface{}
	}
	
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "AllowedMethod/POST",
			args: args{
				method:  http.MethodPost,
				path:    "/rpc/Service/GoodMethod",
				payload: nil,
			},
			want: http.StatusOK,
		},
		{
			name: "DisallowedMethods",
			args: args{
				method:  http.MethodGet,
				path:    "/rpc/Service/BadMethod",
				payload: nil,
			},
			want: http.StatusNotFound,
		},
		{
			name: "NonRoute",
			args: args{
				method:  http.MethodPost,
				path:    "/rpc/Service/BadMethod",
				payload: nil,
			},
			want: http.StatusNotFound,
		},
	}
	
	refTransport, _ := New(Defaults())
	r := Route{
		ServiceName: "Service",
		ActionName:  "GoodMethod",
		Method:      "POST",
		Handler:     func(w http.ResponseWriter, r *http.Request) {},
	}
	refTransport.Register(r)
	
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			
			w := httptest.NewRecorder()
			r := httptest.NewRequest(
				tt.args.method,
				tt.args.path,
				strings.NewReader(`{"service":"HTTP Atom"}`),
			)
			
			refTransport.ServeHTTP(w, r)
			
			require.Equal(t, tt.want, w.Code)
		})
	}
}
