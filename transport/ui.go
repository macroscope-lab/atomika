package transport

import (
	"net/http"
)

// UI allows adding ui support to the project.
// this will mount the handler on "/" (root) path and will
// overwrite anything else if present
// This UI mounting style is designed to work with SPA/WASM app style
// To activate UI
// - Pass a relative/abs path in DIR
// or
// - Pass an FS files system http.FS(fs.Sub())
type UI struct {
	Dir string
	FS  http.FileSystem
}

func (ui *UI) Handler() http.Handler {
	if ui.Dir != "" {
		return http.FileServer(http.Dir(ui.Dir))
	}
	
	if ui.FS != nil {
		return http.FileServer(ui.FS)
	}
	
	return &emptyHandler{}
}

func NewWithEmptyHandler() *UI {
	return &UI{}
}
