package types

import "fmt"

type TypeKey int

func (tk TypeKey) String() string {
	return fmt.Sprintf("%d", tk)
}

const (
	CtxTraceKey TypeKey = iota
)
