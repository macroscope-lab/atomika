package types

import (
	"encoding/hex"
	
	gouuid "github.com/gofrs/uuid"
)

type (
	UUID [16]byte
)

func (u UUID) String() string {
	buf := make([]byte, 36)
	encodeCanonicalUUID(buf, u)
	return string(buf)
}

func GenUUIDv4() UUID {
	uuid, _ := gouuid.NewV4()
	return UUID(uuid)
}

func ValidateUUIDV4(uuid string) error {
	if _, err := gouuid.FromString(uuid); err != nil {
		return err
	}
	return nil
}

// encodeCanonicalUUID replicates UUID type received from package
// this wrapper is taken from https://github.com/gofrs/uuid/blob/master/uuid.go#L205
func encodeCanonicalUUID(buf []byte, u UUID) {
	buf[8] = '-'
	buf[13] = '-'
	buf[18] = '-'
	buf[23] = '-'
	
	hex.Encode(buf[0:8], u[0:4])
	hex.Encode(buf[9:13], u[4:6])
	hex.Encode(buf[14:18], u[6:8])
	hex.Encode(buf[19:23], u[8:10])
	hex.Encode(buf[24:], u[10:])
}
